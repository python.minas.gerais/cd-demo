import pytest
import app


@pytest.fixture
def client():
    app.app.config['TESTING'] = True
    client = app.app.test_client()
    yield client


def test_empty_db(client):
    rv = client.get('/')
    assert b'Hello World - from production' in rv.data
